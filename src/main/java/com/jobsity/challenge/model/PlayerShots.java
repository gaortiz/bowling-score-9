package com.jobsity.challenge.model;

import java.util.List;

public class PlayerShots {

    private String player;
    private List<String> shots;

    protected PlayerShots(String player, List<String> shots) {
        this.player = player;
        this.shots = shots;
    }

    public static PlayerShots createPlayerShots(String player, List<String> shots) {
        return new PlayerShots(player, shots);
    }

    public String getPlayer() {
        return player;
    }

    public List<String> getShots() {
        return shots;
    }

    @Override
    public String toString() {
        return "PlayerShots{" +
                "player='" + player + '\'' +
                ", shots=" + shots +
                '}';
    }
}
